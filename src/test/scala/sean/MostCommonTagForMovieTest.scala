package sean

import com.spotify.scio.testing._

class MostCommonTagForMovieTest extends PipelineSpec {

  val inData = Seq("123::456::tagA::1214944808", "999::456::tagA::1214944808", "486::456::tagB::1214944808", "448::457::tagA::1214944808")
      val expected = Seq("(tagA,2)")

      "MostCommonTagForMovie" should "work" in {
    JobTest[sean.MostCommonTagForMovie.type]
        .args("--tags=tags.dat", "--output=topTags.dat", "--movieID=456")
        .input(TextIO("tags.dat"), inData)
        .output(TextIO("topTags.dat"))(_ should containInAnyOrder (expected))
        .run()
  }

}
