package sean.common

object TestData {
  val MOVIES = Seq(
      "4225::Dish, The (2001)::Comedy",
      "4226::Memento (2000)::Crime|Drama|Mystery|Thriller",
      "4246::Bridget Jones's Diary (2001)::Comedy|Drama|Romance",
      "2::Jumanji (1995)::Adventure|Children|Fantasy",
      "5::Father of the Bride Part II (1995)::Comedy",
      "6::Heat (1995)::Action|Crime|Thriller",
      "16::Casino (1995)::Crime|Drama"
      );
  val RATINGS = Seq(
      "7::4226::5::1049764500",
      "7::4246::3::1050098319",
      "8::2::2.5::1115858432",
      "8::5::3::1116550582",
      "8::6::4::1116547028",
      "8::16::3::1115859664",
      "8::4226::3.5::1115859665"
      );
}