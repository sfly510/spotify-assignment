package sean

import com.spotify.scio.testing._
import sean.common.TestData

class AverageGenreRatingForRaterTest extends PipelineSpec {

  val expectedSeven = Seq("(Thriller,5.0)", "(Crime,5.0)", "(Mystery,5.0)", "(Drama,4.0)", "(Romance,3.0)", "(Comedy,3.0)");
  val expectedEight = Seq("(Crime,3.5)", "(Mystery,3.5)", "(Thriller,3.75)", "(Fantasy,2.5)", "(Action,4.0)", "(Comedy,3.0)", "(Drama,3.25)", "(Adventure,2.5)", "(Children,2.5)");

  "AverageGenreRatingRaterSeven" should "work" in {
    JobTest[sean.AverageGenreRatingForRater.type]
        .args("--movies=movies.dat", "--ratings=ratings.dat", "--output=genreRatingsSeven.dat", "--raterID=7")
        .input(TextIO("movies.dat"), TestData.MOVIES)
        .input(TextIO("ratings.dat"), TestData.RATINGS)
        .output(TextIO("genreRatingsSeven.dat"))(_ should containInAnyOrder (expectedSeven))
        .run()
  }

  "AverageGenreRatingRaterEight" should "work" in {
    JobTest[sean.AverageGenreRatingForRater.type]
        .args("--movies=movies.dat", "--ratings=ratings.dat", "--output=genreRatingsEight.dat", "--raterID=8")
        .input(TextIO("movies.dat"), TestData.MOVIES)
        .input(TextIO("ratings.dat"), TestData.RATINGS)
        .output(TextIO("genreRatingsEight.dat"))(_ should containInAnyOrder (expectedEight))
        .run()
  }

}
