package sean

import com.spotify.scio.testing._
import sean.common.TestData

class MostCommonGenreForRaterTest extends PipelineSpec {

  val expectedSeven = Seq("(Drama,2)", "(Crime,1)", "(Mystery,1)", "(Thriller,1)", "(Comedy,1)", "(Romance,1)");
  val expectedEight = Seq("(Crime,3)", "(Drama,2)", "(Mystery,1)", "(Thriller,2)", "(Adventure,1)", "(Children,1)", "(Fantasy,1)", "(Comedy,1)", "(Action,1)");


  "MostCommonGenreForRaterSeven" should "work" in {
    JobTest[sean.MostCommonGenreForRater.type]
        .args("--movies=movies.dat", "--ratings=ratings.dat", "--output=topGenresSeven.dat", "--raterID=7")
        .input(TextIO("movies.dat"), TestData.MOVIES)
        .input(TextIO("ratings.dat"), TestData.RATINGS)
        .output(TextIO("topGenresSeven.dat"))(_ should containInAnyOrder (expectedSeven))
        .run()
  }

  "MostCommonGenreForRaterEight" should "work" in {
    JobTest[sean.MostCommonGenreForRater.type]
        .args("--movies=movies.dat", "--ratings=ratings.dat", "--output=topGenresEight.dat", "--raterID=8")
        .input(TextIO("movies.dat"), TestData.MOVIES)
        .input(TextIO("ratings.dat"), TestData.RATINGS)
        .output(TextIO("topGenresEight.dat"))(_ should containInAnyOrder (expectedEight))
        .run()
  }

}
