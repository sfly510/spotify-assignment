package sean

import com.spotify.scio._
import scala.util.matching.Regex

/*
sbt runMain sean.AverageGenreRatingForRater
  --ratings=ratings.dat
  --movies=movies.dat
  --raterID=19
  --output=genreRatings
 */

object AverageGenreRatingForRater {
  /**
   * Outputs to a file the average Rating for each Genre rated by a given Rater
   */
  def main(cmdlineArgs: Array[String]): Unit = {
      val (sc, args) = ContextAndArgs(cmdlineArgs);

      val ratings = args("ratings");
      val movies = args("movies");
      val raterID = args("raterID");
      val output = args("output");

      val raterIdRatingsPattern = raterID + "::";

      val movieIdToRating = sc.textFile(ratings)
          .filter(_.startsWith(raterIdRatingsPattern))
          .map(line => ratingToMovieIdRatingTuple(line.split("::")));

      val movieIdToGenres = sc.textFile(movies)
          .map(line => movieToMovieIdGenresTuple(line.split("::")));

      movieIdToGenres.join(movieIdToRating)
      .flatMap(tup => splitIntoGenreRatingTuples(tup))
      .reduceByKey {
      case ((rating1, count1), (rating2, count2)) =>
      (rating1 + rating2, count1 + count2)
      }
      .mapValues {
      case (ratingTotal, count) =>
      ratingTotal.toDouble / count.toDouble
      }
      .saveAsTextFile(output);

      val result = sc.close().waitUntilFinish();
  }

  def ratingToMovieIdRatingTuple(lineParts: Array[String]): Tuple2[String, Double] = {
      (lineParts(1), lineParts(2).toDouble);
  }

  def movieToMovieIdGenresTuple(lineParts: Array[String]): Tuple2[String, Array[String]] = {
      (lineParts(0), lineParts(2).split('|'));
  }

  def splitIntoGenreRatingTuples(
      movieIdToGenresRating: Tuple2[String, Tuple2[Array[String], Double]]):
        Array[Tuple2[String, Tuple2[Double, Int]]] = {
            val rating = movieIdToGenresRating._2._2;
            for (genre <- movieIdToGenresRating._2._1) yield (genre, (rating, 1));
  }
}
