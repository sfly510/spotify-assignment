package sean

import com.spotify.scio._

/*
sbt runMain sean.MostCommonTagForMovie
  --tags=tags.dat
  --movieID=19
  --output=topTags
 */

object MostCommonTagForMovie {
  /**
   * Outputs to a file the most common Tag for a Movie
   */
  def main(cmdlineArgs: Array[String]): Unit = {
      val (sc, args) = ContextAndArgs(cmdlineArgs);

      val tags = args("tags");
      val movieID = args("movieID");
      val output = args("output");

      sc.textFile(tags)
      .map(line => line.split("::").drop(1).take(2))
      .filter(dataArr => dataArr(0).equals(movieID))
      .map(_(1))
      .countByValue
      .max(Ordering[Long].on[(String,Long)](_._2))
      .saveAsTextFile(output);

      val result = sc.close().waitUntilFinish();
  }
}
