package sean

import com.spotify.scio._
import scala.util.matching.Regex

/*
sbt runMain sean.MostCommonGenreForRater
  --ratings=ratings.dat
  --movies=movies.dat
  --raterID=19
  --output=topGenre
 */

object MostCommonGenreForRater {
  /**
   * Outputs to a file the most common Genre for a Rater
   */
  def main(cmdlineArgs: Array[String]): Unit = {
      val (sc, args) = ContextAndArgs(cmdlineArgs);

      val ratings = args("ratings");
      val movies = args("movies");
      val raterID = args("raterID");
      val output = args("output");

      val raterIdRatingsPattern = raterID + "::";

      val movieIdToUserId = sc.textFile(ratings)
          .filter(_.startsWith(raterIdRatingsPattern))
          .map(line => ratingToMovieIdUserIdTuple(line.split("::")));

      val movieIdToGenres = sc.textFile(movies)
          .map(line => movieToMovieIdGenresTuple(line.split("::")));

      movieIdToUserId.join(movieIdToGenres)
      .flatMap(tup => tup._2._2)
      .countByValue
      .saveAsTextFile(output);

      val result = sc.close().waitUntilFinish();
  }

  def ratingToMovieIdUserIdTuple(lineParts: Array[String]): Tuple2[String, String] = {
      (lineParts(1), lineParts(0));
  }

  def movieToMovieIdGenresTuple(lineParts: Array[String]): Tuple2[String, Array[String]] = {
      (lineParts(0), lineParts(2).split('|'));
  }
}
