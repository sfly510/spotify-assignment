import sbt._
import Keys._

val scioVersion = "0.4.0-beta1"
val scalaMacrosVersion = "2.1.0"

lazy val commonSettings = Defaults.coreDefaultSettings ++ packAutoSettings ++ Seq(
  organization          := "sean",
  // Semantic versioning http://semver.org/
  version               := "0.1.0-SNAPSHOT",
  scalaVersion          := "2.11.11",
  scalacOptions         ++= Seq("-target:jvm-1.8",
                                "-deprecation",
                                "-feature",
                                "-unchecked"),
  javacOptions          ++= Seq("-source", "1.8",
                                "-target", "1.8",
                                "-Dsun.io.serialization.extendedDebugInfo=true")
)

lazy val paradiseDependency =
  "org.scalamacros" % "paradise" % scalaMacrosVersion cross CrossVersion.full
lazy val macroSettings = Seq(
  libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value,
  addCompilerPlugin(paradiseDependency)
)

lazy val noPublishSettings = Seq(
  publish := {},
  publishLocal := {},
  publishArtifact := false
)

lazy val root: Project = Project(
  "sean",
  file("."),
  settings = commonSettings ++ macroSettings ++ noPublishSettings ++ Seq(
    description := "sean",
    libraryDependencies ++= Seq(
      "com.spotify" %% "scio-core" % scioVersion,
      "org.slf4j" % "slf4j-simple" % "1.7.13",
      "com.spotify" %% "scio-test" % scioVersion % "test"
    )
  )
)

lazy val repl: Project = Project(
  "repl",
  file(".repl"),
  settings = commonSettings ++ macroSettings ++ noPublishSettings ++ Seq(
    description := "Scio REPL for sean",
    libraryDependencies ++= Seq(
      "com.spotify" %% "scio-repl" % scioVersion
    ),
    mainClass in Compile := Some("com.spotify.scio.repl.ScioShell")
  )
).dependsOn(
  root
)
